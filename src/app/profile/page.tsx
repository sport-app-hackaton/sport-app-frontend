'use client';

import { useEffect } from 'react';
import { Box, Button, FormControl, FormLabel, Input, Text, VStack, HStack, Divider } from '@chakra-ui/react';
import { useAuth } from '~/lib/helpers/AuthContext';
import { redirect } from 'next/navigation';

const Profile = () => {
  const { user, isAuthenticated, isLoading, getUserInfo, logout } = useAuth();

  useEffect(() => {
    if (!isLoading && !isAuthenticated) {
      window.location.href = '/login';
    } else {
      getUserInfo();
    }
  }, [isAuthenticated]);

  if (!user) {
    return <Text>Loading...</Text>;
  }

  return (
    <Box
      w="100%"
      minH="100vh"
      p={8}
      bg="gray.100"
    >
      <Box
        bg="white"
        p={8}
        borderRadius="md"
        boxShadow="md"
        maxW="1200px"
        mx="auto"
      >
        <Text fontSize="3xl" fontWeight="bold" mb={6}>
          Профиль пользователя
        </Text>
        <Divider mb={6} />
        <VStack spacing={4} align="stretch">
          <HStack spacing={4}>
            <FormControl>
              <FormLabel>Email</FormLabel>
              <Input type="email" value={user.email} isReadOnly />
            </FormControl>
            <FormControl>
              <FormLabel>Имя</FormLabel>
              <Input type="text" value={user.first_name} isReadOnly />
            </FormControl>
            <FormControl>
              <FormLabel>Фамилия</FormLabel>
              <Input type="text" value={user.last_name} isReadOnly />
            </FormControl>
          </HStack>
          <HStack spacing={4}>
            <FormControl>
              <FormLabel>Отчество</FormLabel>
              <Input type="text" value={user.middle_name} isReadOnly />
            </FormControl>
            <FormControl>
              <FormLabel>Дата рождения</FormLabel>
              <Input type="date" value={user.bdate} isReadOnly />
            </FormControl>
            <FormControl>
              <FormLabel>Телефон</FormLabel>
              <Input type="tel" value={user.phone_number} isReadOnly />
            </FormControl>
          </HStack>
          <HStack spacing={4}>
            <FormControl>
              <FormLabel>Местоположение</FormLabel>
              <Input type="text" value={user.location} isReadOnly />
            </FormControl>
          </HStack>
          <HStack spacing={4}>
            <FormControl>
              <FormLabel>Пол</FormLabel>
              <Input type="text" value={user.is_male ? 'Мужчина' : 'Женщина'} isReadOnly />
            </FormControl>
            <FormControl>
              <FormLabel>Роль</FormLabel>
              <Input type="text" value={user.role} isReadOnly />
            </FormControl>
          </HStack>
          <Divider my={6} />
          <HStack spacing={4} justify="flex-end">
            <Button colorScheme="teal" as="a" href="/events/add">
              Добавить мероприятие
            </Button>
            <Button colorScheme="teal" as="a" href="/change-password">
              Сменить пароль
            </Button>
            <Button colorScheme="red" onClick={logout}>
              Выйти
            </Button>
          </HStack>
        </VStack>
      </Box>
    </Box>
  );
};

export default Profile;