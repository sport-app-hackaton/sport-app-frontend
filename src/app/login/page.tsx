'use client'
import { useState } from 'react';
import { Box, Button, Flex, FormControl, FormLabel, Input, Link, Text, VStack } from '@chakra-ui/react';
import { useAuth } from '~/lib/helpers/AuthContext';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { login } = useAuth();

  const handleLogin = async (e: React.FormEvent) => {
    e.preventDefault();
    login(email, password);
    window.location.href = '/profile';
  };

  return (
    <Box
      w="100%"
      h="80vh"
      display="flex"
      alignItems="center"
      justifyContent="center"
    >
      <Box
        p={6}
        maxW="400px"
        w="100%"
        bg="white"
        boxShadow="md"
        borderRadius="md"
      >
        <form onSubmit={handleLogin}>
          <VStack spacing={4}>
            <Text fontSize="2rem">Вход</Text>
            <FormControl id="email" isRequired>
              <FormLabel>Email</FormLabel>
              <Input
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormControl>
            <FormControl id="password" isRequired>
              <FormLabel>Пароль</FormLabel>
              <Input
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormControl>
            <Button type="submit" colorScheme="teal" width="full">
              Вход
            </Button>
            <Flex gap="1em">
              <Text>Нет аккаунта?</Text>
              <Link href="/register">Зарегистрироваться</Link>
            </Flex>
          </VStack>
        </form>
      </Box>
    </Box>
  );
};

export default Login;