import { Flex } from '@chakra-ui/react';
import News from '~/lib/components/blocks/News';

const Home = () => {
  return (
    <Flex
      direction="column"
      alignItems="center"
      minHeight="80vh"
      gap={4}
      mb={8}
      w="full"
    >
      <News />
    </Flex>
  );
};

export default Home;
