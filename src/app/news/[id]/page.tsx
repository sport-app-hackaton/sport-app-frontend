'use client';

import { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'next/navigation';
import { Box, Flex, Text, Image, Spinner } from '@chakra-ui/react';

type NewsItem = {
  id: number;
  title: string;
  content: string;
  tag_id: number;
};

const NewsDetail = () => {
  const { id } = useParams();
  const [newsItem, setNewsItem] = useState<NewsItem | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    if (id) {
      const fetchNewsItem = async () => {
        try {
          const response = await axios.get(`http://127.0.0.1:8000/news/${id}`);
          setNewsItem(response.data);
          setIsLoading(false);
        } catch (error) {
          console.error('Error fetching news item:', error);
          setIsLoading(false);
        }
      };

      fetchNewsItem();
    }
  }, [id]);

  if (isLoading) {
    return (
      <Flex width="full" height="100vh" alignItems="center" justifyContent="center">
        <Spinner size="xl" />
      </Flex>
    );
  }

  if (!newsItem) {
    return (
      <Flex width="full" height="100vh" alignItems="center" justifyContent="center">
        <Text>Новость не найдена</Text>
      </Flex>
    );
  }

  return (
    <Box w="100%" p={8} bg="gray.100" minH="100vh">
      <Box bg="white" p={8} borderRadius="md" boxShadow="md" maxW="800px" mx="auto">
        <Image
          src="/img/newsPlaceholder.png"
          alt={newsItem.title}
          borderTopRadius="md"
          width="full"
          height="50vh"
          objectFit="cover"
        />
        <Box p={4}>
          <Text fontSize="2xl" fontWeight="bold" mb={4}>
            {newsItem.title}
          </Text>
          <Text>{newsItem.content}</Text>
        </Box>
      </Box>
    </Box>
  );
};

export default NewsDetail;