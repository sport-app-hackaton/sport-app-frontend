'use client';

import { useState } from 'react';
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Link,
  Text,
  VStack,
  RadioGroup,
  Radio,
} from '@chakra-ui/react';
import { useAuth } from '~/lib/helpers/AuthContext';
const Register = () => {
  const [email, setEmail] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [middleName, setMiddleName] = useState('');
  const [bdate, setBdate] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [photoPath, setPhotoPath] = useState('');
  const [location, setLocation] = useState('');
  const [isMale, setIsMale] = useState(true);
  const [role, setRole] = useState('');
  const [password, setPassword] = useState('');
  const { register } = useAuth();

  const handleRegister = async (e: React.FormEvent) => {
    e.preventDefault();

    const userData = {
      email,
      first_name: firstName,
      last_name: lastName,
      middle_name: middleName,
      bdate,
      phone_number: phoneNumber,
      photo_path: photoPath,
      location,
      is_male: isMale,
      role,
      password,
    };

    try {
      await register(userData);
    } catch (error) {
      alert(error);
    }
  };

  return (
    <Box
      w="100%"
      //h="80vh"
      display="flex"
      alignItems="center"
      justifyContent="center"
    >
      <Box
        p={6}
        maxW="400px"
        w="100%"
        bg="white"
        boxShadow="md"
        borderRadius="md"
      >
        <form onSubmit={handleRegister}>
          <VStack spacing={4}>
            <Text fontSize="2rem">Регистрация</Text>
            <FormControl id="email" isRequired>
              <FormLabel>Email</FormLabel>
              <Input
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormControl>
            <FormControl id="first_name" isRequired>
              <FormLabel>Имя</FormLabel>
              <Input
                type="text"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              />
            </FormControl>
            <FormControl id="last_name" isRequired>
              <FormLabel>Фамилия</FormLabel>
              <Input
                type="text"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
            </FormControl>
            <FormControl id="middle_name">
              <FormLabel>Отчество</FormLabel>
              <Input
                type="text"
                value={middleName}
                onChange={(e) => setMiddleName(e.target.value)}
              />
            </FormControl>
            <FormControl id="bdate" isRequired>
              <FormLabel>Дата рождения</FormLabel>
              <Input
                type="date"
                value={bdate}
                onChange={(e) => setBdate(e.target.value)}
              />
            </FormControl>
            <FormControl id="phone_number" isRequired>
              <FormLabel>Телефон</FormLabel>
              <Input
                type="tel"
                value={phoneNumber}
                onChange={(e) => setPhoneNumber(e.target.value)}
              />
            </FormControl>
            <FormControl id="location" isRequired>
              <FormLabel>Местоположение</FormLabel>
              <Input
                type="text"
                value={location}
                onChange={(e) => setLocation(e.target.value)}
              />
            </FormControl>
            <FormControl id="is_male" isRequired>
              <FormLabel>Пол</FormLabel>
              <RadioGroup
                onChange={(value) => setIsMale(value === 'true')}
                value={isMale.toString()}
              >
                <Flex direction="row" gap="1em">
                  <Radio value="true">Мужчина</Radio>
                  <Radio value="false">Женщина</Radio>
                </Flex>
              </RadioGroup>
            </FormControl>
            <FormControl id="role" isRequired>
              <FormLabel>Роль</FormLabel>
              <RadioGroup onChange={(value) => setRole(value)} value={role}>
                <VStack align="start">
                  <Radio value="athlete">Спортсмен</Radio>
                  <Radio value="coach">Тренер</Radio>
                  <Radio value="official">Спортивный функционер</Radio>
                </VStack>
              </RadioGroup>
            </FormControl>
            <FormControl id="password" isRequired>
              <FormLabel>Пароль</FormLabel>
              <Input
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormControl>
            <Button type="submit" colorScheme="teal" width="full">
              Зарегистрироваться
            </Button>
            <Flex gap="1em">
              <Text>Уже есть аккаунт?</Text>
              <Link href="/login">Войти</Link>
            </Flex>
          </VStack>
        </form>
      </Box>
    </Box>
  );
};

export default Register;
