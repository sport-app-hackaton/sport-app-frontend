'use client';

import { useState } from 'react';
import { Box, Button, FormControl, FormLabel, Input, VStack, Select } from '@chakra-ui/react';
import axios from 'axios';
import dynamic from 'next/dynamic';
import 'react-quill/dist/quill.snow.css';

// Использование dynamic import для React Quill, чтобы избежать проблем с SSR
const ReactQuill = dynamic(() => import('react-quill'), { ssr: false });

const AddEvent = () => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [date, setDate] = useState('');
  const [location, setLocation] = useState('');
  const [tagId, setTagId] = useState('');

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    const eventData = {
      title,
      description,
      date,
      location,
      tag_id: parseInt(tagId, 10),
      owner_token: localStorage.getItem('token')
    };

    try {
      await axios.post('http://127.0.0.1:8000/events', eventData, {
        headers: {
          'Content-Type': 'application/json',
        },
      });
      window.location.href = '/events'; // Перенаправление на страницу со списком ивентов после успешного добавления
    } catch (error) {
      console.error('Failed to add event', error);
    }
  };

  return (
    <Box
      w="100%"
      minH="100vh"
      p={8}
      bg="gray.100"
      display="flex"
      alignItems="center"
      justifyContent="center"
    >
      <Box
        bg="white"
        p={8}
        borderRadius="md"
        boxShadow="md"
        maxW="600px"
        w="100%"
      >
        <form onSubmit={handleSubmit}>
          <VStack spacing={4} align="stretch">
            <FormControl isRequired>
              <FormLabel>Название</FormLabel>
              <Input
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </FormControl>
            <FormControl isRequired>
              <FormLabel>Описание</FormLabel>
              <ReactQuill
                value={description}
                onChange={setDescription}
                theme="snow"
              />
            </FormControl>
            <FormControl isRequired>
              <FormLabel>Дата</FormLabel>
              <Input
                type="date"
                value={date}
                onChange={(e) => setDate(e.target.value)}
              />
            </FormControl>
            <FormControl isRequired>
              <FormLabel>Местоположение</FormLabel>
              <Input
                type="text"
                value={location}
                onChange={(e) => setLocation(e.target.value)}
              />
            </FormControl>
            <FormControl isRequired>
              <FormLabel>Тег</FormLabel>
              <Select
                placeholder="Выберите тег"
                value={tagId}
                onChange={(e) => setTagId(e.target.value)}
              >
                <option value="0">Шахматы</option>
                <option value="1">Футбол</option>
                <option value="2">Баскетбол</option>
                <option value="4">Хоккей</option>
                <option value="5">Волейбол</option>
              </Select>
            </FormControl>
            <Button type="submit" colorScheme="teal" width="full">
              Добавить мероприятие
            </Button>
          </VStack>
        </form>
      </Box>
    </Box>
  );
};

export default AddEvent;