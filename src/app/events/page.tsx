import { Flex } from '@chakra-ui/react';
import Events from '~/lib/components/blocks/Events';

const Home = () => {
  return (
    <Flex
      direction="column"
      alignItems="center"
      minHeight="80vh"
      gap={4}
      mb={8}
      w="full"
    >
      <Events />
    </Flex>
  );
};

export default Home;
