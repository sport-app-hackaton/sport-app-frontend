import { Box, Button, Flex, Image, Text, Grid } from '@chakra-ui/react';
import { ArrowForwardIcon } from '@chakra-ui/icons';
import { useAuth } from '~/lib/helpers/AuthContext';

import ThemeToggle from './ThemeToggle';

const Header = () => {
  const { isAuthenticated, isLoading, login, logout } = useAuth();

  return (
    <Flex
      as="header"
      width="full"
      align="center"
      paddingX="2rem"
      paddingY="0.3rem"
      borderBottomRadius="0.5em"
      backgroundColor="white"
    >
      <Grid alignItems="center" width="full" gap="1rem" gridAutoFlow="column">
        <Flex
          as="a"
          href="/"
          alignItems="center"
          gap="0.5rem"
          justifySelf="start"
        >
          <Image src="/img/logo.png" boxSize="5rem"></Image>
          <Text fontSize="2em">Citius</Text>
        </Flex>
        <Flex gap="1em" fontSize="1.2rem" justifySelf="start">
          <Text as="a" href="/news">
            Новости
          </Text>
          <Text as="a" href="/events">
            Мероприятия
          </Text>
          <Text as="a" href="/calendar">
            Календарь
          </Text>
        </Flex>
        <Flex justifySelf="end" gap="1rem" fontSize="1.2rem">
          {!isLoading && !isAuthenticated && (
            <Button
              as="a"
              href="/login"
              fontSize="1.2rem"
              fontWeight="500"
              backgroundColor="transparent"
              leftIcon={<ArrowForwardIcon />}
            >
              Вход
            </Button>
          )}
          {!isLoading && isAuthenticated && (
            <Flex as="a" href="/profile">
              <Image src="/img/avatarPlaceholder.png" boxSize="3rem" borderRadius="full"/>
            </Flex>
          )
          }
        </Flex>
      </Grid>
    </Flex>
  );
};

export default Header;
