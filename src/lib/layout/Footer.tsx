import { Flex, Link, Text } from '@chakra-ui/react';

const Footer = () => {
  return (
    <Flex as="footer" width="full" justifyContent="center">
      <Text fontSize="sm">
        {new Date().getFullYear()} -{' '}
        <Link href="https://www.iate.obninsk.ru/" isExternal rel="noopener noreferrer">
          OINPE NRNU MEPhI
        </Link>
      </Text>
    </Flex>
  );
};

export default Footer;
