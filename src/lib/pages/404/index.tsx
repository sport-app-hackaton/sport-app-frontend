import {
  Box,
  Button,
  Heading,
  Image,
  Text,
  Link as ChakraLink,
  Flex,
} from '@chakra-ui/react';
import Link from 'next/link';

import MotionBox from '~/lib/components/motion/Box';

const Page404 = () => {
  return (
    <Flex minHeight="80vh" direction="column" justifyContent="center">
      <MotionBox
        animate={{ y: 20 }}
        transition={{ repeat: Infinity, duration: 2, repeatType: 'reverse' }}
        width={{ base: '100%', sm: '70%', md: '60%' }}
        margin="0 auto"
      >
        <Image
          src="/404 Error-pana.svg"
          alt="Error 404 not found Illustration"
        />
      </MotionBox>

      <Box marginY={4}>
        <Heading textAlign="center" size="lg">
          Страница не найдена.
        </Heading>

        <Box textAlign="center" marginTop={4}>
          <Button as={Link} href="/" size="sm">
            Вернуться на главную
          </Button>
        </Box>
      </Box>
    </Flex>
  );
};

export default Page404;
