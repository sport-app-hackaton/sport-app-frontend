'use client'
import { createContext, useState, useContext, ReactNode, useEffect } from 'react';
import { redirect } from 'next/navigation';
import axios from 'axios';

interface User {
  email: string;
  first_name: string;
  last_name: string;
  middle_name: string;
  bdate: string;
  phone_number: string;
  photo_path: string;
  location: string;
  is_male: boolean;
  role: string;
}

interface AuthContextType {
  isAuthenticated: boolean;
  isLoading: boolean;
  user: User | null;
  login: (email: string, password: string) => Promise<void>;
  register: (userData: RegisterData) => Promise<void>;
  logout: () => void;
  getUserInfo: () => Promise<void>;
}

interface RegisterData {
  email: string;
  first_name: string;
  last_name: string;
  middle_name: string;
  bdate: string;
  phone_number: string;
  photo_path: string;
  location: string;
  is_male: boolean;
  role: string;
  password: string;
}

const AuthContext = createContext<AuthContextType | undefined>(undefined);

export const AuthProvider = ({ children }: { children: ReactNode }) => {
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [user, setUser] = useState<User | null>(null);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const token = localStorage.getItem('token');
      if (token) {
        setIsAuthenticated(true);
        getUserInfo().finally(() => setIsLoading(false));
      } else {
        setIsLoading(false);
      }
    }
  }, []);

  const login = async (email: string, password: string) => {
    try {
      const response = await axios.post('http://127.0.0.1:8000/users/login/', { email, password });
      localStorage.setItem('token', response.data.personal_token);
      setIsAuthenticated(true);
      getUserInfo();
      redirect('/profile');
    } catch (error) {
      throw new Error('Invalid credentials');
    }
  };

  const register = async (userData: RegisterData) => {
    try {
      const response = await axios.post('https://127.0.0.1:8000/api/auth/register', userData);
      localStorage.setItem('token', response.data.personal_token);
      setIsAuthenticated(true);
      getUserInfo();
      redirect('/profile');
    } catch (error) {
      throw new Error('Registration failed');
    }
  };

  const logout = () => {
    localStorage.removeItem('token');
    setIsAuthenticated(false);
    setUser(null);
    redirect('/login');
  };

  const getUserInfo = async () => {
    try {
      const response = await axios.get(`http://127.0.0.1:8000/users/${localStorage.getItem('token')}`, {
      });
      setUser(response.data);
    } catch (error) {
      console.error('Failed to fetch user info');
    }
  };

  return (
    <AuthContext.Provider value={{ isAuthenticated, isLoading, user, login, register, logout, getUserInfo }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider');
  }
  return context;
};