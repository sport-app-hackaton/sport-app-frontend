'use client';

import { useEffect, useState } from 'react';
import axios from 'axios';
import { Flex, Image, Text, IconButton, Spinner } from '@chakra-ui/react';
import { FaTh } from 'react-icons/fa';
import { TfiLayoutSliderAlt } from "react-icons/tfi";
import MotionBox from '../motion/Box';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

type EventItem = {
  id: number;
  title: string;
  description: string;
  date: string;
  location: string;
  tag_id: number;
  owner_id: number;
};

const Event = (data: EventItem) => {
  return (
    <MotionBox
      key={data.id}
      backgroundColor="white"
      borderRadius="0.5rem"
      width="90%"
      height="30vh"
      boxShadow="md"
      as="a"
      href={`/events/${data.id}`}
      display="flex"
      flexDirection="column"
      whileHover={{
        scale: 1.05,
        transition: { duration: 0.2 },
      }}
      whileTap={{ scale: 0.95 }}
    >
      <Image
        src="/img/eventPlaceholder.png"
        borderTopRadius="0.5rem"
        width="full"
        height="50%"
        objectFit="cover"
      />
      <Flex flexDirection="column" padding="0.5rem" marginTop="auto">
        <Text>{data.title}</Text>
        <Text fontSize="0.8rem">{new Date(data.date).toLocaleDateString('ru-RU')}</Text>
      </Flex>
    </MotionBox>
  );
};

const Events = () => {
  const [viewMode, setViewMode] = useState<'tile' | 'slider'>('tile');
  const [events, setEvents] = useState<EventItem[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchEvents = async () => {
      try {
        const response = await axios.get('http://127.0.0.1:8000/events/?skip=0&limit=100');
        setEvents(response.data);
        setIsLoading(false);
      } catch (error) {
        console.error('Error fetching events:', error);
        setIsLoading(false);
      }
    };

    fetchEvents();
  }, []);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  };

  if (isLoading) {
    return (
      <Flex width="full" height="100vh" alignItems="center" justifyContent="center">
        <Spinner size="xl" />
      </Flex>
    );
  }

  return (
    <Flex width="full" flexDirection="column">
      <Flex justifyContent="space-between" alignItems="center" mb="1rem">
        <Text fontSize="32">Мероприятия</Text>
        <Flex>
          <IconButton
            icon={<FaTh />}
            onClick={() => setViewMode('tile')}
            aria-label="Tile view"
            mr="1rem"
          />
          <IconButton
            icon={<TfiLayoutSliderAlt />}
            onClick={() => setViewMode('slider')}
            aria-label="Slider view"
          />
        </Flex>
      </Flex>
      {viewMode === 'tile' ? (
        <Flex
          borderRadius="0.5em"
          width="full"
          flexWrap="wrap"
          gap="1rem"
        >
          {events.map((item) => (
            <MotionBox
              key={item.id}
              backgroundColor="white"
              borderRadius="0.5rem"
              width="18.9%"
              height="30vh"
              boxShadow="md"
              as="a"
              href={`/events/${item.id}`}
              display="flex"
              flexDirection="column"
              whileHover={{
                scale: 1.05,
                transition: { duration: 0.2 },
              }}
              whileTap={{ scale: 0.95 }}
            >
              <Image
                src="/img/eventPlaceholder.png"
                borderTopRadius="0.5rem"
                width="full"
                height="50%"
                objectFit="cover"
              />
              <Flex flexDirection="column" padding="0.5rem" marginTop="auto">
                <Text>{item.title}</Text>
                <Text fontSize="0.8rem">{new Date(item.date).toLocaleDateString('ru-RU')}</Text>
              </Flex>
            </MotionBox>
          ))}
        </Flex>
      ) : (
        <Slider {...settings}>
          {events.map((item) => (
            <Event key={item.id} {...item} />
          ))}
        </Slider>
      )}
    </Flex>
  );
};

export default Events;