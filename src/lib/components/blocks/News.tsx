'use client';

import { useEffect, useState } from 'react';
import axios from 'axios';
import { Flex, Image, Text, IconButton, Spinner } from '@chakra-ui/react';
import { FaTh } from 'react-icons/fa';
import { TfiLayoutSliderAlt } from "react-icons/tfi";
import MotionBox from '../motion/Box';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

type NewsItem = {
  id: number;
  title: string;
  content: string;
  tag_id: number;
};

const New = ({ id, title, content }: NewsItem) => {
  return (
    <MotionBox
      key={id}
      backgroundColor="white"
      borderRadius="0.5rem"
      width="90%"
      height="30vh"
      boxShadow="md"
      as="a"
      href={`/news/${id}`}
      display="flex"
      flexDirection="column"
      whileHover={{
        scale: 1.05,
        transition: { duration: 0.2 },
      }}
      whileTap={{ scale: 0.95 }}
    >
      <Image
        src={"/img/newsPlaceholder.png"}
        borderTopRadius="0.5rem"
        width="full"
        height="50%"
        objectFit="cover"
      />
      <Flex flexDirection="column" padding="0.5rem" marginTop="auto">
        <Text>{title}</Text>
      </Flex>
    </MotionBox>
  );
};

const News = () => {
  const [viewMode, setViewMode] = useState<'tile' | 'slider'>('slider');
  const [news, setNews] = useState<NewsItem[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchNews = async () => {
      try {
        const response = await axios.get('http://127.0.0.1:8000/news/?skip=0&limit=100');
        setNews(response.data);
        setIsLoading(false);
      } catch (error) {
        console.error('Error fetching news:', error);
        setIsLoading(false);
      }
    };

    fetchNews();
  }, []);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  };

  if (isLoading) {
    return (
      <Flex width="full" height="100vh" alignItems="center" justifyContent="center">
        <Spinner size="xl" />
      </Flex>
    );
  }

  return (
    <Flex width="full" flexDirection="column">
      <Flex justifyContent="space-between" alignItems="center" mb="1rem">
        <Text fontSize="32">Новости</Text>
        <Flex>
          <IconButton
            icon={<FaTh />}
            onClick={() => setViewMode('tile')}
            aria-label="Tile view"
            mr="1rem"
          />
          <IconButton
            icon={<TfiLayoutSliderAlt />}
            onClick={() => setViewMode('slider')}
            aria-label="Slider view"
          />
        </Flex>
      </Flex>
      {viewMode === 'tile' ? (
        <Flex
          borderRadius="0.5em"
          width="full"
          flexWrap="wrap"
          gap="1rem"
        >
          {news.map((item) => (
            <MotionBox
              key={item.id}
              backgroundColor="white"
              borderRadius="0.5rem"
              width="18.9%"
              height="30vh"
              boxShadow="md"
              as="a"
              href={`/news/${item.id}`}
              display="flex"
              flexDirection="column"
              whileHover={{
                scale: 1.05,
                transition: { duration: 0.2 },
              }}
              whileTap={{ scale: 0.95 }}
            >
              <Image
                src={"/img/newsPlaceholder.png"}
                borderTopRadius="0.5rem"
                width="full"
                height="50%"
                objectFit="cover"
              />
              <Flex flexDirection="column" padding="0.5rem" marginTop="auto">
                <Text>{item.title}</Text>
              </Flex>
            </MotionBox>
          ))}
        </Flex>
      ) : (
        <Slider {...settings}>
          {news.map((item) => (
            <New key={item.id} {...item} />
          ))}
        </Slider>
      )}
    </Flex>
  );
};

export default News;